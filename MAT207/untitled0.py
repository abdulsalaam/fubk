#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 12 20:51:22 2022

@author: abdulsalaam
"""

import numpy as np
xnot = 2
def f(xn):
    return xn + np.log(xn) -2

def fprime(xn):
    return 1 + 1.0/xn
xn = xnot
for i in range(10):
    xn = xnot
    xn1 = xn - f(xn)/fprime(xn)
    print(xn1)
    if(np.abs(xn - xn1) <= 0.00005):
        break
    xnot = xn1


def f(xn):
    return np.e**(-xn) + np.tan(xn)
xn = 1
xn1 = 0.7
for i in range(5):
    xn2 = xn1 - f(xn1)*((xn1 - xn)/(f(xn1) - f(xn)))
    print(xn, xn1, xn2)
    xn = xn1
    xn1 = xn2


x = np.array([0,1,2,3,4])
y = np.array([-4,-1,4,11,20])

x = np.arange(0,1.1,.10)
y = np.e**(-x)
